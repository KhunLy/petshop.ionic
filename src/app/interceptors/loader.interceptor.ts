import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable, Subject, from } from 'rxjs';
import { finalize, takeUntil, mergeMap } from 'rxjs/operators';
import { LoadingController } from '@ionic/angular';

@Injectable()
export class LoaderInterceptor implements HttpInterceptor {

  constructor(private loadingController: LoadingController) { }

  intercept(req: HttpRequest<any>, next: HttpHandler)
    : Observable<HttpEvent<any>> {
    if(req.reportProgress) {
      return next.handle(req);
    }
    return from(this.loadingController.create({
      message: 'Please wait ...'
    })).pipe(mergeMap((loader) => {
      loader.present();
      return next.handle(req).pipe(finalize(() => {
        // workaround to wait at least 500 milliseconds
        setTimeout(() => loader.dismiss(), 500);
        // loader.dismiss();
      }));
    }))
    

  }

}