import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BreedModel } from '../models/breed.model';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BreedService {

  constructor(
    private httpClient: HttpClient
  ) { }

  getAll(): Observable<BreedModel[]> {
    return this.httpClient.get<BreedModel[]>(environment.apiUrl + '/breed');
  }

  getAllByAnimal(animalName: string) : Observable<BreedModel[]>{
    return this.httpClient.get<BreedModel[]>(environment.apiUrl + '/breed/byAnimal/' + animalName);
  }

  insert(model: BreedModel): Observable<BreedModel> {
    return this.httpClient.post<BreedModel>(environment.apiUrl + '/breed', model);
  }

  delete(id: number): Observable<any> {
    return this.httpClient.delete(environment.apiUrl + '/breed/' + id);
  }
}
