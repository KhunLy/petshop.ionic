import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { TokenModel } from '../models/token.model';
import * as jwt_decode from 'jwt-decode';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class SessionService {
  private _isLogged$ : BehaviorSubject<boolean>;
  
  public get isLogged$() : BehaviorSubject<boolean> {
    return this._isLogged$;
  }  
  
  private _token$ : BehaviorSubject<string>;

  public get token$() : BehaviorSubject<string> {
    return this._token$;
  }

  private _tokenModel$ : BehaviorSubject<TokenModel>;

  public get tokenModel$() : BehaviorSubject<TokenModel> {
    return this._tokenModel$;
  }

  constructor(
    private storage: Storage
  ) {
    this.storage.get('TOKEN_MODEL').then(data => {
      let connectedUser = <TokenModel>data;
      let date = new Date();
      let timestamp = date.getTime();
      this._isLogged$ = new BehaviorSubject<boolean>(
        connectedUser != null && connectedUser.exp - timestamp/1000 > 0
      );
      this._tokenModel$ = new BehaviorSubject<TokenModel>(connectedUser);
    });
    this.storage.get('TOKEN').then(data => {
      this._token$ = new BehaviorSubject<string>(data);
    });
  }

  start(token: string) {
    this.storage.set('TOKEN', token);
    this._token$.next(token);

    let tokenObj =  jwt_decode(token);
    this.storage.set('TOKEN_MODEL', tokenObj);
    this._tokenModel$.next(tokenObj);

    this._isLogged$.next(true);
  }

  clear() {
    this.storage.clear();
    this._token$.next(null);
    this._tokenModel$.next(null);
    this._isLogged$.next(false);
  }
}
