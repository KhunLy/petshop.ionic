import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { PetModel } from '../models/pet.model';
import { Observable } from 'rxjs';
import { PetInsertModel } from '../models/pet-insert.model';

@Injectable({
  providedIn: 'root'
})
export class PetService {

  constructor(
    private httpClient: HttpClient,
  ) { }

  getAllByAnimal(name: string): Observable<PetModel[]>{
    return this.httpClient.get<PetModel[]>(environment.apiUrl + '/Pet/byAnimal/' + name);
  }

  getPetsByUser(id: number): Observable<PetModel[]> {
    return this.httpClient.get<PetModel[]>(environment.apiUrl + '/Pet/byUser/' + id);
  }

  insert(model: PetInsertModel): Observable<any> {
    return this.httpClient.post(environment.apiUrl + '/Pet', model);
  }

  update(model: PetInsertModel): Observable<any> {
    return this.httpClient.put(environment.apiUrl + '/Pet', model);
  }

  book(id: number) {
    return this.httpClient.patch(environment.apiUrl + '/Pet/book/' + id, null);
  }

  cancel(id: number) {
    return this.httpClient.patch(environment.apiUrl + '/Pet/cancel/' + id, null);
  }

  get(id: number): Observable<PetModel> {
    return this.httpClient.get<PetModel>(environment.apiUrl + '/Pet/' + id);
  }

}
