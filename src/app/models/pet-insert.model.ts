export interface PetInsertModel {
    id?: number;
    reference: string;
    birthDate: Date;
    breedId: number;
    image: string;
    imageMimeType: string;
    vaccinated: boolean
    description?: string
    petStatusName: string;
    userId?: number;
    breedName: string;
    animalName: string;
}