export interface RegisterModel {
  email: string;
  password: string;
  lastName: string;
  firstName: string;
  birthDate?: Date;
}