import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/services/session.service';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  formGroup: FormGroup;

  constructor(
    private auth: AuthService,
    private router: Router,
    private toastrService: ToastController,
    private session: SessionService,
  ) { }

  ngOnInit() {
    this.formGroup = new FormGroup({
      'email' : new FormControl(null, Validators.compose([
        Validators.required
      ])),
      'password' : new FormControl(null, Validators.compose([
        Validators.required
      ])),
    });
  }

  submit() {
    this.auth.login(this.formGroup.value).subscribe(data => {
      this.session.start(data);
      this.router.navigateByUrl('/').then(async () => {
        let toast = await this.toastrService.create({
          message: "Welcome " + this.session.tokenModel$.value.FirstName,
          duration: 2000
        });
        toast.present();
      });
    }, async (xhr) => {
      let toast = await this.toastrService.create({
        message: xhr.message,
        duration: 2000
      });
      toast.present();
    }, () => {

    });
  }

}
